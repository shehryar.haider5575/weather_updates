1. This Project is developed by using the technology of Laravel Framework, based on check Weather Updates by a given city. 
2. I am using Open Weather Map JSON API (http://openweathermap.org/api) to get real weather data.
3. In case of not found weather updates by a given city the exception will be handled and through an error message on a      
   specified page.
4. Getting Weather Updates by using GUZZLE HTTP ^6.0 which is used to get data through third party API's.
5. I am using Admin Panel Theme to setup the frontend with responsive layout.  
