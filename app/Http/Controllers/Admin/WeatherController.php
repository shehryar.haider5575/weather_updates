<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use GuzzleHttp\Exception\RequestException;

class WeatherController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('weather.check-update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable()
    {
        $url    = storage_path().'/app/city.json';
        
        // Read Content From Specified File
        $cities = file_get_contents($url);
        $data   = json_decode($cities, true);
        return DataTables::of($data)->make();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $city_id
     * @return Renderable
     */
    public function checkWeatherUpdate(Request $request)
    {
        $data = [];
        if(isset($request->name) && !empty($request->name))
        {
            $client     =   new \GuzzleHttp\Client();
                
            $URI = 'api.openweathermap.org/data/2.5/weather?q='.$request->name.'&units=metric&appid='.env('APP_ID');
            
            try {
                $response = $client->get($URI, [
                    'headers' => [
                        'Accept' => 'application/json'
                    ]
                ]);

                $response_data =    $response->getBody();

                $response_data =    json_decode($response_data);
                $data = [
                    'city'      => $request->city,    
                    'weather'   => $response_data,    
                ];

            } catch (RequestException $e) {
                
                $data = [
                    'error' => 'Could not find weather of this city: ',
                    'city'  => $request->name,
                ];
            }
        }
        return view('weather.check-update',$data);
    }
 }
