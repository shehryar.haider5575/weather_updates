@extends('layouts.master')
@section('title', __('Weather Updates'))
@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <span>{{ __('Weather Updates')}}</span>
    </li>
</ul>
@endsection

<!-- BEGIN Category STATS 1-->
<!-- BEGIN Weather Updates STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-cloud"> </i> {{ __('Weather Updates')}}
                    </div>
                    <div class="panel-body">
                        @if (isset($error))
                            <p class="alert alert-danger">{{$error}} <b style="font-size:16px;">{{$city}}</b></p>
                        @endif

                        <div class="custom_datatable">
                            <span class="text-danger" id="error"></span>
                            <form action="{{route('weather.update')}}">
                                <div class="bg-black-transparent1 m-b-15 p15 pb0">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">{{ __('City Name')}}</label>
                                                <input type="text" name="name" id="autocomplete-ajax1"  class="form-control p_name" placeholder="Search By City Name" style="  z-index: 2;" utocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <br>
                                            <div class="form-group" style="margin-top: 5px">
                                                <button type="submit" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                <i class="fa fa-submit pr-1"></i> {{ __('Check Weather') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                 </div>
                                <div class="portlet-body">
                                    @if (isset($weather) && !empty($weather))
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div id="card" class="weater">
                                                    <div class="city-selected">
                                                        <article>
                                
                                                            <div class="info">
                                                                <div class="city"><span>City:</span> {{$weather->name}}</div> <span class="cloud">{{$weather->weather[0]->main}}</span>
                                                                <br>
                                                                <div class="night">{{$weather->weather[0]->description}} - {{\Carbon\Carbon::parse(now())->format('h:i a')}}</div>
                                
                                                                <div class="temp">{{$weather->main->temp}}°</div>
                                
                                                                <div class="wind">
                                                                    <svg version="1.1" id="wind" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                        viewBox="0 0 300.492 300.492" style="enable-background:new 0 0 300.492 300.492;" xml:space="preserve">
                                
                                                                    <span class="col-sm-8">{{$weather->wind->speed}} meter/sec</span> 
                                                                    <span class="col-sm-8">Humidity {{$weather->main->humidity}} %</span>
                                                                </div>
                                                            </div>
                                
                                                            <div class="icon">
                                                            <img src="http://openweathermap.org/img/wn/{{$weather->weather[0]->icon}}@2x.png" alt="{{$weather->weather[0]->main}}" height="100px">
                                                            </div>
                                
                                                        </article>
                                                        
                                                        <figure style="background-image: url(http://136.243.1.253/~creolitic/bootsnipp/home.jpg)"></figure>
                                                    </div>
                                
                                                    <div class="days">
                                                        <div class="row row-no-gutter">
                                                            <div class="col-md-4">
                                                                <div class="day">
                                                                    <h1>Feels Like</h1>
                                                                    <p>{{$weather->main->feels_like}}°</p>
                                                                </div>
                                                            </div>
                                
                                                            <div class="col-md-4">
                                                                <div class="day">
                                                                    <h1>Temp Min</h1>
                                                                    <p>{{$weather->main->temp_min}}°</p>
                                                                </div>
                                                            </div>
                                
                                                            <div class="col-md-4">
                                                                <div class="day">
                                                                    <h1>Temp Max</h1>
                                                                    <p>{{$weather->main->temp_max}}°</p>
                                                                </div>
                                                            </div>
                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection

