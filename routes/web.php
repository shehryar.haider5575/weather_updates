<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// ROUTE::Weather Update
Route::get('/', 'Admin\WeatherController@index')->name('check_weather');
Route::get('/weather-update/{city_id?}', 'Admin\WeatherController@checkWeatherUpdate')->name('weather.update');

Route::get('clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('storage:link');
    return ('done');
});